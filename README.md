# hashanalyzer

Hash Analyzer Fortinet Project

Instructions to run:
The app is live at: https://hashanalyzerlive.herokuapp.com/

To run locally:

1. Install dependencies found in requirements.txt by running the command
$ pip3 install -r requirements.txt

2. Then at the project root folder containing manage.py, run
$ python manage.py runserver

Using the app:
After navigating to https://hashanalyzerlive.herokuapp.com/, click the Browse button to 
upload a text file containing hashs then click the Upload button.This will use the VirusTotal 
API to get the SHA-256 identifier of the file. Next the app will check if a report for this file 
already exists and will serve that report. Otherwise, it will use the API to get the latest report.


Project File layout:
The project root contains all the folders, misc text files needed to run the project
on Heroku such as requirements.txt and runtime.txt and manage.py which is used
to change Django settings and to run the app. The staticfiles folder is where css files 
are stored. The media folder is used to store the text files that are uploaded. The
hashanalyzer folder contains the settings and db management used by Django.
The analyzer table contains the actual app where the templates(html files), routing
and logic such as forms and models are.

Tech Stack:
Windows, Python-django, MySQL-Heroku ClearDB, Heroku

Dependencies:
requests, pymysql, django-cleanup, moment, urllib3, full list found in requirements.txt

Issues:
The main issues were learning new technologies and fulfilling the two goals outlined. This was my first time using
Django,Heroku and containers so time was spent getting used to these technologies. The two goals were a way to let
users come back later to see the report and storing the results locally to reduce queries. For the first goal, I 
implemented a previously scanned page which showed the reports of files previously scanned. This will allow users to
scan multiple files and review the reports at a later date. For the second goal, the report is saved and added to
a list of objects. If a query is made and the SHA-256 identifier matches an object then there is a check if the query is older
than 1 day. If its older, the old object is deleted and VirusTotal api will be called for a new report. If it is not older, the
object is returned and no further api calls are made. A particular issue related to the api is there is a maximum of 4 requests per minute
which caused errors in the app due to the supposed hash file not existing. I thought about adding a delay but instead used try/except blocks
and passed when a request was unavailable.
