import requests
from .models import HashData
from django.forms.models import model_to_dict
import moment
from datetime import datetime

api_key = '75854e9bbfe0ae2fe1296db2a8ea406514610509ec23d2104fa8a4018c4a4773'
api_base_url = url = 'https://www.virustotal.com/vtapi/v2/file'

def get_hash(hash):
    params = {'apikey': api_key}
    last = hash.objects.last()
    url = api_base_url + '/scan'
    try:
        files = {'file': (last.filename, open(last.filename, 'rb'))}
        input_response = requests.post(url, files=files, params=params)
        input_json = input_response.json()
    except:
        print('Error Getting Hash')
        return None
    sha_256_key = input_json['sha256']
    return upload_hash(sha_256_key)



def upload_hash(key):
    url = api_base_url + '/report'
    params = {'apikey': api_key, 'resource': key}
    hash_exist = None
    try:
        hash_exist = HashData.objects.get(hash_value=key)
    except:
        pass
    if hash_exist:
        date_scanned = moment.date(hash_exist.scan_date[:10], "%Y-%m-%d")
        date_scanned = date_scanned.format('YYYY-MM-DD')
        current = moment.now().format('YYYY-MM-DD')
        d1 = datetime.strptime(current, "%Y-%m-%d")
        d2 = datetime.strptime(date_scanned, "%Y-%m-%d")
        diff = (abs((d2 - d1).days))
        if diff > 1:
            hash_exist.delete()
        else:
            return model_to_dict(hash_exist)
    try:
        output_response = requests.get(url, params=params)
        output_json = output_response.json()
        if output_json['scans']['Fortinet']['result'] == None:
            name = 'None'
        else:
            name = output_json['scans']['Fortinet']['result']
        result = HashData(hash_value=output_json['sha256'], detection_name=name,
                          engine_number=len(output_json['scans']), scan_date=output_json['scan_date'])
        result.save()
    except:
        print('Error Uploading Hash')
        return None

    hash = HashData.objects.get(hash_value=key)
    return model_to_dict(hash)