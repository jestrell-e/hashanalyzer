from django.shortcuts import render, redirect
from .forms import HashForm
from .models import TextHash, HashData
from .api_calls import get_hash


# Create your views here.

def main(request):
    json_data = None
    hash = TextHash
    if hash:
        json_data = get_hash(hash)
    if request.method == 'POST':
        form = HashForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return redirect('/')
    else:
        form = HashForm()
    if json_data:
        return render(request, 'main.html', {'form': form, 'sha256': json_data['hash_value'], 'length': json_data['engine_number'],'fortinet': json_data['detection_name'], 'date': json_data['scan_date']})
    else:
        return render(request, 'main.html', {'form': form})


def previously_scanned(request):
    hash_data = HashData.objects.all()
    return render(request, 'show_previous.html',{'hash_data': hash_data})