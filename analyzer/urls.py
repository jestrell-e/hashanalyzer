from django.urls import path
from . import views

urlpatterns = [
    path('', views.main,name='main'),
    path('previous/',views.previously_scanned, name='previous')
]