from django import forms
from .models import TextHash

class HashForm(forms.ModelForm):
    class Meta:
        model = TextHash
        fields = ('Upload_Text_File',)
