from django.db import models

# Create your models here.
class TextHash(models.Model):
    Upload_Text_File = models.FileField(upload_to='text/',)

    @property
    def filename(self):
        return self.Upload_Text_File.path

class HashData(models.Model):
    hash_value = models.CharField(max_length=64,default='None')
    detection_name = models.CharField(max_length=40,default='None',null=True)
    engine_number = models.CharField(max_length=40,default='0')
    scan_date = models.CharField(max_length=20,default='None')

    def __str__(self):
        return self.hash_value
